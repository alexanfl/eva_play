# params: {'input_array': 'list[number]'}

import pandas as pd
import numpy as np

N = 10
np.random.seed(1)
columns = ['col OUTPUT'] + [f'col {i}' for i in range(1, N)]
df = pd.DataFrame(columns=columns, data=np.random.uniform(-1, 1, (N, N)))

df['col OUTPUT'] = df['col OUTPUT']*np.array(input_array)
df
