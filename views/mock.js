function ngConstructor() {
    this.defaultArray = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
    this.inputArray = this.defaultArray;
    this.allowedIndices = [7, 8, 9];
    this.getStyle = getStyle.bind(this);
    this.arrayOfObjects = [];
    this.dataSourceColumns = [];

    this.updateArray = function() {
        let newArray = [];
        $('input', $('#input-table')).each((i, elem) => {
            const currentVal = $(elem)[0].value;
            newArray.push(currentVal)
        });

        const checkIfNumber = (x) => !isNaN(parseFloat(x));
        if (newArray.every(checkIfNumber)) {
            this.inputArray = newArray;
        }
        else {
            alert('not all values are numbers')
        }
    }
}

function ngAfterViewInit() {
    const that = this;

    this.ds = this.getStatDataSource('ds');

    this.ds.stateChange.subscribe(function(change) {
        if (change.newstate === 'data') {
            that.dataSourceColumns = that.ds.data.getAllColumnNames();
            that.arrayOfObjects = that.ds.data.getDataAsObjects(that.dataSourceColumns);
        }
    });
}

function getStyle(cellVal) {
    if (cellVal > 0.67) {
        return {
            'background-color': '#AA2626',
            'color': 'white'
        }
    }
    else if (cellVal > 0.33 && cellVal <= 0.67) {
        return {
            'background-color': '#CD4F4F',
            'color': 'white'
        }
    }
    else if (cellVal > 0) {
        return {
            'background-color': '#E57070',
            'color': 'white'
        }
    }
    else if (cellVal < -0.67) {
        return {
            'background-color': '#44AC24',
            'color': 'white'
        }
    }
    else if (cellVal >= -0.67 && cellVal < -0.33) {
        return {
            'background-color': '#68CB4A',
            'color': 'white'
        }
    }
    else if (cellVal < 0) {
        return {
            'background-color': '#8BE570',
            'color': 'white'
        }
    }

    return {}
}